import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EplanningComponent } from './eplanning/eplanning.component';
import { MenuComponent } from './menu/menu.component';
import { CateringComponent } from './catering/catering.component';
import { EdecorComponent } from './edecor/edecor.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactComponent } from './contact/contact.component';
import { OcateringComponent } from './ocatering/ocatering.component';

const routes: Routes = [
  { path:'',redirectTo:'/home',pathMatch:'full' },
  { path:'home',component:HomeComponent, data: {animation: 'homepage'} },
  { path:'eventsplanning',component:EplanningComponent, data: {animation: 'eventplanning'} },
  { path:'menu',component:MenuComponent,data: { animation: 'menu' } },
  { path:'catering',component:CateringComponent },
  { path:'eventdecor',component:EdecorComponent },
  { path:'gallery',component:GalleryComponent },
  { path:'contact',component:ContactComponent },
  { path:'outdoorcatering',component:OcateringComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
