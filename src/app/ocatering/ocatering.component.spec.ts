import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OcateringComponent } from './ocatering.component';

describe('OcateringComponent', () => {
  let component: OcateringComponent;
  let fixture: ComponentFixture<OcateringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OcateringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OcateringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
