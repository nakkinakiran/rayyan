import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EplanningComponent } from './eplanning.component';

describe('EplanningComponent', () => {
  let component: EplanningComponent;
  let fixture: ComponentFixture<EplanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EplanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EplanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
