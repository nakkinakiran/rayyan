import { Component, OnInit } from '@angular/core';
import { trigger,transition,state,style,animate } from '@angular/animations';
import { Product } from '../entities/product.entity';
import { Mobile } from '../entities/mobile.entity';

@Component({
  selector: 'app-gallery',  
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
  animations: [
    trigger('openClose',[
      state('open',style({
        height: '200px',
        opacity: 1,
        backgroundColor: 'yellow'
      })),
      state('close',style({
        height: '100px',
        opacity: 0.5,
        backgroundColor: 'red'
      })),
      transition('open => close',[
        animate('1s')
      ]),
      transition('close => open',[
        animate('.5s')
      ]),      
    ]),
  ]
})
export class GalleryComponent implements OnInit {

  isOpen = true;
  name: string;
  age: number;
  status: boolean;
  email: any;

  products: Product;
  mobiles: Mobile;

  result: String= '';
  txtresult: String= '';
  catresult: string= '';
  months = ['january','february','march','april','may','june','july','august','september','october','november','december'];
  isAvailable = false;
  gallerytitle= 'gallerytext';
 
  toggle() {
    this.isOpen = !this.isOpen;
  }

 constructor() { }

  ngOnInit() {
    this.name = "kiran";
    this.age =  24;
    this.status = true;
    this.email = 'kiran@gmail.com';

    this.products = [
      {
        id: 'p1',
        pname: 'Azouma',
        photo: 'a-logo.png',
        price: 150,
        quantity: 1
      },
      {
        id: 'p2',
        pname: 'Icare',
        photo: 'i-logo.png',
        price: 250,
        quantity: 5
      },
      {
        id: 'p3',
        pname: 'Blucart',
        photo: 'blu-logo.png',
        price: 300,
        quantity: 3
      },
  ];
  this.mobiles= [
    {
      mobileid: 'm1',
      mobilepic: 'a10.png',
      mobilename: 'samsung galaxy a10(blue, 32GB)',
      mobileprice: 7990
    },
    {
      mobileid: 'm2',
      mobilepic: 'a20.png',
      mobilename: 'samsung galaxy a20(deep blue, 32GB)',
      mobileprice: 11490
    },
    {
      mobileid: 'm3',
      mobilepic: 'm30.png',
      mobilename: 'samsung galaxy m30(blue, 64GB)',
      mobileprice: 16490
    },
    {
      mobileid: 'm4',
      mobilepic: 'm10.png',
      mobilename: 'samsung galaxy m10(blue, 16GB)',
      mobileprice: 8190
    }
  ]
  }
  clickme(): void{
    this.result = 'Hello World';
  }
  display(event: any): void{
    this.txtresult= 'Hello..!'+ event.target.value;
    this.catresult= 'category is selected'+ event.target.value;
  }
}
