export class Product{
    id: string;
    pname: string;
    photo: string;
    price: number;
    quantity: number;
}