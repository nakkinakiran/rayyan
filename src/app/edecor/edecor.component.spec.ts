import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdecorComponent } from './edecor.component';

describe('EdecorComponent', () => {
  let component: EdecorComponent;
  let fixture: ComponentFixture<EdecorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdecorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdecorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
